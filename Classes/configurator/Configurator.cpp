#include "Configurator.h"
#include "cocos2d.h"
#include <string>
#include <sstream>
#include <fstream>

using namespace config;
using namespace cocos2d;
using namespace std;

Configurator *Configurator::pInstance = nullptr;

Configurator::Configurator() {
	initFromFile("config/options.cfg");
}

Configurator::~Configurator() {

}

bool Configurator::initFromFile(const string& fName) {
	initDefault();
	return true;
}

void Configurator::saveToFile(const string& fName) {
	
}

cocos2d::Label* Configurator::initLabel() {
	TTFConfig config;
	config.outlineSize = 1;
	config.fontFilePath = "fonts/komika.ttf";
	config.fontSize = 26;
	label = Label::createWithTTF(config, "0");
	label->setAnchorPoint(Vec2(0, 1));
	label->setPosition(Vec2(Director::getInstance()->getVisibleSize().width / 2,
		Director::getInstance()->getVisibleSize().height));
	label->setVerticalAlignment(TextVAlignment::TOP);
	label->setHorizontalAlignment(TextHAlignment::CENTER);
	label->setColor(Color3B(255, 230, 183));
	return label;
}

void Configurator::initDefault() {
	animationTime = 0.1f;
	cloudScale = 1.0f;
	birdAverageHeight = 58.59f;
	waveTimer = 2.5f;
	birdsMaxAmount = 6;
	birdsStartAmount = 1;
	_points = 0;
}

bool Configurator::gotInTable(const string& fName) {
	vector<std::pair<char*, int>> table;
	FILE* fp =
		std::fopen(fName.c_str(), "r");
	if (!fp) return false;

	char buf[255] = "";
	for (int i = 0; i < 10; i++) {
		int a;
		std::fscanf(fp, "%s", buf);
		std::fscanf(fp, "%d", &a);
		table.push_back(std::pair<char*, int>(buf, a));
	}
	std::sort(table.begin(), table.end(), [](auto &left, auto &right) {
		return left.second > right.second;
	});

	auto i = min_element(table.begin(), table.end());
	if (config::Configurator::getInstance()->getPoints() > i->second) {
		fclose(fp);
		return true;
	}
	fclose(fp);
	return false;		
}