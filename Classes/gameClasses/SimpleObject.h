#ifndef __SIMPLE_OBJECT_H__
#define __SIMPLE_OBJECT_H__

#include <string>
#include "cocos2d.h"
#include "configurator\Configurator.h"

class SimpleObject : public cocos2d::Sprite {

protected:
	SimpleObject() {}
	SimpleObject(const SimpleObject&) {}
public:
	virtual ~SimpleObject() {}
	void setSpeed(const float& speed) { _speed = speed; }
	const float& getSpeed() const { return _speed; }
	virtual void startMove() = 0;

protected:
	cocos2d::Vector<cocos2d::Animation*> _animations;
	cocos2d::Action* _currentAnimation = nullptr;
	cocos2d::Action* _currentAction = nullptr;
	float _speed;
	
protected:
	static cocos2d::Animation* _createAnimation(const std::string& model, const int& type,
		const int& frames, float animationTime = config::Configurator::getInstance()->animationTime);
};

#endif // __SIMPLE_OBJECT_H__
